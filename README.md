# SOCRA_Project


Group members:
- Guillaume Pedrona, guillaume.pedrona
- Antony Yang, antony.yang
- Alexandre Hu, alexandre.hu
- Thibault Cavaciuti, thibault.cavaciuti

# Installation Required

- Docker

# How to launch the project

- In a terminal, run:
```
docker-compose up --build -d
```

# How to use the project

## Get Request

- "/mission/all": Get all missions in JSON form.
- "/mission/\{id\}": Get the mission of id 'id' in JSON form.
- "/mission/filter?keywords=word1,word2": Get all missions filtered and sorted by the words 'word1' and 'word2'.
- "/mission/export/\{id\}": Export the mission of id 'id' as a PDF file.

## Post Request

- "/mission/add": Add a new mission from a JSON form.
- "/mission/\{id\}": Edit the mission of id 'id'.

Example of json for all the post requests:
```
{
    "location" : "Paris",
    "duration" : "1 ANS",
    "rate" : "1600 euros",
    "telecommuting" : "50%",
    "start" : "ASAP",
    "post" : "Développeur Frontend",
    "context" : "",
    "mission" : "Créer un frontend pour un backend existant."
}
```
