package fr.epita.socra_project;

import java.awt.Color;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import fr.epita.socra_project.data.model.MissionModel;


public class MissionPdfExporter {

    private final MissionModel mission;

    public MissionPdfExporter(MissionModel mission) {
        this.mission = mission;
    }

    /**
     * @brief method for exporting one mission according to the response in argument
     * @param response: the response we receive from localhost
     * @throws DocumentException: throws an error if there is a problem during the creation or the edition of the document.
     * @throws IOException: throws an error if there is a problem.
     */
    public void export(HttpServletResponse response) throws DocumentException, IOException
    {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();
        Font font = FontFactory.getFont(FontFactory.HELVETICA_BOLD);
        font.setSize(18);
        font.setColor(Color.BLACK);

        Paragraph p = new Paragraph("Mission description", font);

        p.add("\n\nLocation: " + mission.getLocation());
        p.add("\nDuration: " + mission.getDuration());
        p.add("\nRate: " + mission.getRate());
        p.add("\nTelecommuting: " + mission.getTelecommuting());
        p.add("\nStart: " + mission.getStart());
        p.add("\nPost: " + mission.getPost());
        p.add("\nContext: " + mission.getContext());
        p.add("\nMission: " + mission.getMission());

        document.add(p);

        document.close();
    }
}