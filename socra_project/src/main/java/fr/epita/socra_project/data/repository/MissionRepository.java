package fr.epita.socra_project.data.repository;

import fr.epita.socra_project.data.model.MissionModel;
import org.springframework.data.repository.CrudRepository;

public interface MissionRepository extends CrudRepository<MissionModel, Integer> {
}
