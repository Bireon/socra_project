package fr.epita.socra_project.data.model;

import lombok.*;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;
import java.util.Objects;

@Entity @Table(name = "Missions")
@EnableAutoConfiguration
@AllArgsConstructor @NoArgsConstructor @Getter @Setter @With
public class MissionModel {
    private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Integer id;
    private String location;
    private String duration;
    private String rate;
    private String telecommuting;
    private String start;
    private String post;
    private String context;
    private String mission;

    public MissionModel(String location,
                        String duration,
                        String rate,
                        String telecommuting,
                        String start,
                        String post,
                        String context,
                        String mission) {

        this.location = location;
        this.duration = duration;
        this.rate = rate;
        this.telecommuting = telecommuting;
        this.start = start;
        this.post = post;
        this.context = context;
        this.mission = mission;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        var obj = (MissionModel) o;

        return Objects.equals(id, obj.id) && Objects.equals(location, obj.location) && Objects.equals(duration, obj.duration)
                && Objects.equals(rate, obj.rate) && Objects.equals(telecommuting, obj.telecommuting)
                && Objects.equals(start, obj.start) && Objects.equals(post, obj.post)
                && Objects.equals(context, obj.context) && Objects.equals(mission, obj.mission);
    }
}
