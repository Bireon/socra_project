package fr.epita.socra_project.presentation.payload;

import lombok.*;

@NoArgsConstructor
@Getter
@Setter
@With
public class EditMissionRequest {
    private String location;
    private String duration;
    private String rate;
    private String telecommuting;
    private String start;
    private String post;
    private String context;
    private String mission;

    public EditMissionRequest(String location,
                              String duration,
                              String rate,
                              String telecommuting,
                              String start,
                              String post,
                              String context,
                              String mission) {
        this.location = location;
        this.duration = duration;
        this.rate = rate;
        this.telecommuting = telecommuting;
        this.start = start;
        this.post = post;
        this.context = context;
        this.mission = mission;
    }
}
