package fr.epita.socra_project.presentation.payload;

import lombok.*;

@AllArgsConstructor @NoArgsConstructor @Getter @Setter @With
public class AddMissionRequest {
    private String location;
    private String duration;
    private String rate;
    private String telecommuting;
    private String start;
    private String post;
    private String context;
    private String mission;
}
