package fr.epita.socra_project.presentation.controller;

import fr.epita.socra_project.MissionPdfExporter;
import fr.epita.socra_project.data.model.MissionModel;
import fr.epita.socra_project.domain.service.MissionService;
import fr.epita.socra_project.presentation.payload.AddMissionRequest;
import fr.epita.socra_project.presentation.payload.EditMissionRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/mission")
public class MissionController {
    @Autowired
    private MissionService missionService;

    /**
     * @brief endpoint for getting all missions.
     * @return list of all missions, in reverse order to have them sorted by the most recent one first.
     */
    @GetMapping("/all")
    public List<MissionModel> getAllMissions() {
        return missionService.getAllMissions();
    }

    /**
     * @brief endpoint for adding a new mission.
     * @param addRequest: a AddMissionRequest with all necessary attributes in order to create a new mission
     */
    @PostMapping("/add")
    public void addMission(@RequestBody AddMissionRequest addRequest) {
        missionService.addMission(addRequest.getLocation(),
                                  addRequest.getDuration(),
                                  addRequest.getRate(),
                                  addRequest.getTelecommuting(),
                                  addRequest.getStart(),
                                  addRequest.getPost(),
                                  addRequest.getContext(),
                                  addRequest.getMission());
    }
    /**
     * @brief endpoint for getting the mission associated to an id
     * @param id: id of the mission to get
     * @return a MissionModel storing every data of the researched mission
     */
    @GetMapping("/{id}")
    public MissionModel getMissionById(@PathVariable int id) {
        return missionService.getMissionById(id);
    }

    /**
     * @brief endpoint for editting a mission according the datas stored in editMissionRequest
     * @param id: the id of the mission to edit
     * @param editMissionRequest: the request storing the new datas for the researched mission
     */
    @PostMapping("/{id}")
    public void editMission(@PathVariable int id, @RequestBody EditMissionRequest editMissionRequest)
    {
        missionService.editMission(id,
                editMissionRequest.getLocation(),
                editMissionRequest.getDuration(),
                editMissionRequest.getRate(),
                editMissionRequest.getTelecommuting(),
                editMissionRequest.getStart(),
                editMissionRequest.getPost(),
                editMissionRequest.getContext(),
                editMissionRequest.getMission());
    }

    /**
     * @brief endpoint for getting and exporting the information of the mission associated to an id in a PDF file
     * @param id: the id of the mission to export
     * @param response: the response we receive from localhost
     * @throws IOException
     */
    @GetMapping("/export/{id}")
    public void exportMissionPdf(@PathVariable int id, HttpServletResponse response) throws IOException
    {
        MissionModel missionModel = missionService.getMissionById(id);
        response.setContentType("application/pdf");

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=mission_" + id + ".pdf";
        response.setHeader(headerKey, headerValue);

        MissionPdfExporter exporter = new MissionPdfExporter(missionModel);
        exporter.export(response);
    }

    /**
     * @brief Function to get a filtered list of missions according to keywords. If no keywords are provided, it is the equivalent of calling getAllMissions().
     * @param keywords: a list of keywords.
     * @return a filtered and sorted list (filtered according to the keywords and sorted according to the number of occurrences of the keywords in each mission, by descending order).
     */
    @GetMapping("/filter")
    public List<MissionModel> getMissionsWithKeywords(@RequestParam(required = false, name = "keywords") String[] keywords) {
        return missionService.getFilteredMissions(keywords);
    }
}
