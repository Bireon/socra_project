package fr.epita.socra_project;

import fr.epita.socra_project.domain.service.MissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class SocraProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocraProjectApplication.class, args);
	}

}

@Component
class MissionAdder implements CommandLineRunner {

	@Autowired
	private MissionService missionService;

	@Override
	public void run(String... args){
		if (missionService.count() == 0) {
			missionService.addMission("Issy-les-Moulineaux",
					"12 MOIS",
					"Tarif non renseigné",
					"100%",
					"ASAP",
					"Développeur ReactJS",
					null,
					"Intégration au sein de l’équipe Engineering du pôle Industrialisation");

			missionService.addMission("Paris",
					"1 MOIS",
					"10000 euros",
					"50%",
					"12 mai 2021",
					"Développeur c++",
					"Projet initiation au C++",
					"Création calculatrice avec grands nombres");

			missionService.addMission("Le Kremlin-Bicêtre",
					"1 MOIS",
					"666 euros",
					"0%",
					"2 novembre 2019",
					"Développeur C",
					"Projet 42sh",
					"Réalisation shell personnel");

			missionService.addMission("Villejuif",
					"3 jours",
					"Rien",
					"100%",
					"3 mars 2020",
					"Développeur Java",
					"Rush Java",
					"Réalisation WebService");

			missionService.addMission("Toulon",
					"1 MOIS",
					"Relaxation",
					"24%",
					"1 août 2020",
					"Etudiant ing2",
					"Vacances",
					"Se reposer au soleil");

			missionService.addMission("Paris",
					"1 AN",
					"1000 euros",
					"24%",
					"5 août 2021",
					"Développeur frontend",
					"Développement site jeu vidéo",
					"Vous allez devoir afficher sur une page web tous les jeux accessibles par les utilisateurs.");

			missionService.addMission("Bordeaux",
					"10 ANS",
					"500 euros",
					"24%",
					"1 août 2012",
					"Bagnard",
					"Emprisonné dans une galère",
					"Vous devez ramer sans vous arrêter pendant des heures.");

			missionService.addMission("Sevran",
					"5 MOIS",
					"10 tickets restaurants",
					"24%",
					"10 août 2021",
					"Développeur backend",
					"Projet SOCRA",
					"Vous allez devoir vous occuper de la connexion avec la base de données pour le projet de SOCRA.");

			missionService.addMission("Paris",
					"7 MOIS",
					"Tarif non renseigné",
					"24%",
					"1 août 2022",
					null,
					"Projet .NET",
					"Créez une application web quelconque en utilisant le framework .NET");

			missionService.addMission("Paris",
					"7 MOIS",
					"Tarif non renseigné",
					"24%",
					"1 août 2022",
					"Unknown",
					null,
					"Unknown");
		}
	}
}