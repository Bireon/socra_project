package fr.epita.socra_project.domain.service;

import fr.epita.socra_project.data.model.MissionModel;
import fr.epita.socra_project.data.repository.MissionRepository;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MissionService {
    @Autowired
    private MissionRepository missionRepository;

    /**
     * @brief function for getting all missions from the database.
     * @return list of all missions, in reverse order to have them sorted by the most recent one first.
     */
    public List<MissionModel> getAllMissions() {
        List<MissionModel> missions = new ArrayList<>();
        missionRepository.findAll().forEach(missions::add);

        Collections.reverse(missions);

        return missions;
    }

    /**
     * @brief function for adding a new mission to the database.
     * @param location: the location of the new mission.
     * @param duration: the duration of the new mission.
     * @param rate: the rate of the new mission.
     * @param telecommuting: the telecommuting of the new mission.
     * @param start: the start of the new mission.
     * @param post: the post of the new mission.
     * @param context: the context of the new mission.
     * @param mission: the mission of the new mission.
     */
    public void addMission(String location,
                           String duration,
                           String rate,
                           String telecommuting,
                           String start,
                           String post,
                           String context,
                           String mission) {
        MissionModel new_mission = new MissionModel(location, duration, rate, telecommuting, start, post, context, mission);
        missionRepository.save(new_mission);
    }

    /**
     * @brief method that takes a mission from the database
     * @param id: the id of the researched mission
     * @return the MissionModel that represents the mission
     */
    public MissionModel getMissionById(Integer id)
    {
        try {
            var mission = missionRepository.findById(id).get();
            return mission;
        }
        catch (Exception e)
        {
            return null;
        }
    }

    /**
     * @brief method that edit the mission with the good id directly in the database
     * @param id: the id of the mission
     * @param location: the new location of the mission
     * @param duration: the new duration of the mission
     * @param rate: the new rate of the mission
     * @param telecommuting: the new telecommuting of the mission
     * @param start: the new start of the mission
     * @param post: the new post of the mission
     * @param context: the new context of the mission
     * @param mission: the new description of the mission
     */
    public void editMission(int id, String location,
                            String duration,
                            String rate,
                            String telecommuting,
                            String start,
                            String post,
                            String context,
                            String mission) {
        try {
            MissionModel missionModel = missionRepository.findById(id).get();
            missionModel.setMission(mission);
            missionModel.setDuration(duration);
            missionModel.setTelecommuting(telecommuting);
            missionModel.setContext(context);
            missionModel.setLocation(location);
            missionModel.setPost(post);
            missionModel.setRate(rate);
            missionModel.setStart(start);

            missionRepository.save(missionModel);
        }
        catch(Exception e)
        {
            throw e;
        }
    }

    /**
     * @brief Function to get a filtered list of missions according to keywords. If no keywords are provided, it is the equivalent of calling getAllMissions().
     * @param keywords: a list of keywords.
     * @return a filtered and sorted list (filtered according to the keywords and sorted according to the number of occurrences of the keywords in each mission, by descending order).
     */
    public List<MissionModel> getFilteredMissions(String[] keywords) {
        List<MissionModel> missions = getAllMissions();

        if (keywords != null && keywords.length > 0) {
            missions = filterList(missions, keywords);
        }

        return missions;
    }

    /**
     * @brief Function to filter a list of missions according to one or more keywords. If no keyword is provided the given list is returned.
     * @param missions: the list of missions (MissionModel).
     * @param keywords: the list of keywords.
     * @return a filtered list sorted according to the number of occurrences of the keywords in each mission.
     */
    public static List<MissionModel> filterList(List<MissionModel> missions, String[] keywords) {
        if (keywords == null || keywords.length == 0) {
            return missions;
        }

        List<String> tmpList = new ArrayList<>(Arrays.asList(keywords));
        tmpList.removeIf(x -> x.equals(""));
        keywords = tmpList.toArray(new String[0]);

        if (keywords.length == 0) {
            return missions;
        }

        List<Pair<MissionModel, Double>> filteredList = new ArrayList<>();

        for (MissionModel mission : missions) {
            double occurrences = 0;
            List<String> alreadyFoundKeywords = new ArrayList<>();

            for (String keyword : keywords) {

                if (mission.getMission() != null) {
                    occurrences += StringUtils.countMatches(mission.getMission().toLowerCase(), keyword.toLowerCase());
                    if (!alreadyFoundKeywords.contains(keyword)) {
                        alreadyFoundKeywords.add(keyword);
                    }
                }

                if (mission.getContext() != null) {
                    occurrences += StringUtils.countMatches(mission.getContext().toLowerCase(), keyword.toLowerCase());
                    if (!alreadyFoundKeywords.contains(keyword)) {
                        alreadyFoundKeywords.add(keyword);
                    }
                }

                if (mission.getDuration() != null) {
                    occurrences += StringUtils.countMatches(mission.getDuration().toLowerCase(), keyword.toLowerCase());
                    if (!alreadyFoundKeywords.contains(keyword)) {
                        alreadyFoundKeywords.add(keyword);
                    }
                }

                if (mission.getLocation() != null) {
                    occurrences += StringUtils.countMatches(mission.getLocation().toLowerCase(), keyword.toLowerCase());
                    if (!alreadyFoundKeywords.contains(keyword)) {
                        alreadyFoundKeywords.add(keyword);
                    }
                }

                if (mission.getPost() != null) {
                    occurrences += StringUtils.countMatches(mission.getPost().toLowerCase(), keyword.toLowerCase());
                    if (!alreadyFoundKeywords.contains(keyword)) {
                        alreadyFoundKeywords.add(keyword);
                    }
                }

                if (mission.getRate() != null) {
                    occurrences += StringUtils.countMatches(mission.getRate().toLowerCase(), keyword.toLowerCase());
                    if (!alreadyFoundKeywords.contains(keyword)) {
                        alreadyFoundKeywords.add(keyword);
                    }
                }

                if (mission.getStart() != null) {
                    occurrences += StringUtils.countMatches(mission.getStart().toLowerCase(), keyword.toLowerCase());
                    if (!alreadyFoundKeywords.contains(keyword)) {
                        alreadyFoundKeywords.add(keyword);
                    }
                }

                if (mission.getTelecommuting() != null) {
                    occurrences += StringUtils.countMatches(mission.getTelecommuting().toLowerCase(), keyword.toLowerCase());
                    if (!alreadyFoundKeywords.contains(keyword)) {
                        alreadyFoundKeywords.add(keyword);
                    }
                }

            }

            //formula: m * o - 0.5 * n * O with m the number of keywords found, n the total number of keywords and o the number of occurrences.
            double occurrenceValue = alreadyFoundKeywords.size() * occurrences - 0.5 * keywords.length * occurrences;
            Pair<MissionModel, Double> pair = Pair.of(mission, occurrenceValue);
            filteredList.add(pair);

        }

        return sortList(filteredList);
    }

    /**
     * @brief Function to sort a list of missions (MissionModel) according to their number of occurrences. Sorted by descending order of occurrences.
     * @param missionsWithOccurrences: the list of missions with their number of occurrences
     * @return a sorted list according to the number of occurrences of each mission.
     */
    private static List<MissionModel> sortList(List<Pair<MissionModel, Double>> missionsWithOccurrences) {
        missionsWithOccurrences.sort((o1, o2) -> {
            if (o1.getSecond() > o2.getSecond()) {
                return -1;
            } else if (o1.getSecond().equals(o2.getSecond())) {
                return 0;
            } else {
                return 1;
            }
        });

        List<MissionModel> orderedMissions = new ArrayList<>();

        for (var mission : missionsWithOccurrences) {
            orderedMissions.add(mission.getFirst());
        }

        return orderedMissions;
    }

    /**
     * @brief function for getting the number of data in the database
     * @return list of all missions, in reverse order to have them sorted by the most recent one first.
     */
    public int count() {
        List<MissionModel> missions = new ArrayList<>();
        missionRepository.findAll().forEach(missions::add);
        return missions.size();
    }
}
