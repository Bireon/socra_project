package fr.epita.socra_project;


import fr.epita.socra_project.data.model.MissionModel;
import fr.epita.socra_project.data.repository.MissionRepository;
import fr.epita.socra_project.domain.service.MissionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
public class SocraProjectApplicationTests {

    @TestConfiguration
    static class Configuration {
        @Bean
        public MissionService missionService() {
            return new MissionService();
        }
    }

    @Autowired
    private MissionService missionService;

    @MockBean
    private MissionRepository missionRepository;

    @Before
    public void setUp() {
        List<MissionModel> list = new ArrayList<>();
        list.add(new MissionModel(1,
                "Paris",
                "1 MOIS",
                "10000 euros",
                "50%",
                "12 mai 2021",
                "Développeur c++",
                "Projet initiation au C++",
                "Création calculatrice avec grands nombres"));
        list.add(new MissionModel(2,
                "Le Kremlin-Bicêtre",
                "1 MOIS",
                "666 euros",
                "0%",
                "2 novembre 2019",
                "Développeur C",
                "Projet 42sh",
                "Réalisation shell personnel"));
        list.add(new MissionModel(3,
                "Villejuif",
                "3 jours",
                "Rien",
                "100%",
                "3 mars 2020",
                "Développeur Java",
                "Rush Java",
                "Réalisation WebService"));

        Mockito.when(missionRepository.findAll())
                .thenReturn(list);

        Mockito.when(missionRepository.findById(1))
                .thenReturn(java.util.Optional.of(list.get(0)));
    }

    void createMockMissionsList(List<MissionModel> missions) {
        missions.add(new MissionModel(1,
                "Issy-les-Moulineaux",
                "12 MOIS",
                "Tarif non renseigné",
                "100%",
                "ASAP",
                "Développeur ReactJS",
                null,
                "Intégration au sein de l’équipe Engineering du pôle Industrialisation"));


        missions.add(new MissionModel(2,
                "Paris",
                "1 MOIS",
                "10000 euros",
                "50%",
                "12 mai 2021",
                "Développeur c++",
                "Projet initiation au C++ avec tests en appliquant les principes du cours socra",
                "Création calculatrice avec grands nombres pour les tests"));

        missions.add(new MissionModel(3,
                "Le Kremlin-Bicêtre",
                "1 MOIS",
                "666 euros",
                "0%",
                "2 novembre 2019",
                "Développeur C",
                "Projet 42sh",
                "Réalisation shell personnel"));

        missions.add(new MissionModel(4,
                "Villejuif",
                "3 jours",
                "Rien",
                "100%",
                "3 mars 2020",
                "Développeur Java",
                "Rush Java",
                "Réalisation WebService"));

        missions.add(new MissionModel(5,
                "Toulon",
                "1 MOIS",
                "Relaxation",
                "24%",
                "1 août 2020",
                "Etudiant ing2",
                "Vacances",
                "Se reposer au soleil"));

        missions.add(new MissionModel(6,
                "Paris",
                "1 AN",
                "1000 euros",
                "24%",
                "5 août 2021",
                "Développeur frontend",
                "Développement site jeu vidéo et tests",
                "Vous allez devoir afficher sur une page web tous les jeux accessibles par les utilisateurs."));

        missions.add(new MissionModel(7,
                "Bordeaux",
                "10 ANS",
                "500 euros",
                "24%",
                "1 août 2012",
                "Bagnard",
                "Emprisonné dans une galère",
                "Vous devez ramer sans vous arrêter pendant des heures."));

        missions.add(new MissionModel(8,
                "Sevran/Paris",
                "5 MOIS",
                "10 tickets restaurants",
                "24%",
                "10 août 2021",
                "Développeur backend",
                "Projet SOCRA",
                "Vous allez devoir vous occuper de la connexion avec la base de données pour le projet de SOCRA."));

        missions.add(new MissionModel(9,
                "Paris",
                "7 MOIS",
                "Tarif non renseigné",
                "24%",
                "1 août 2022",
                null,
                "Projet .NET (avec tests)",
                "Créez une application web quelconque en utilisant le framework .NET ainsi que des tests pour tester cette dernière"));

        missions.add(new MissionModel(10,
                "Paris test",
                "7 MOIS test",
                "Tarif non renseigné test",
                "24% test",
                "1 août 2022 test",
                "Unknown test",
                null,
                "Unknown test"));
    }

    /**
     * UnitTest
     */
    @Test
    public void getAllMission() {
        List<MissionModel> res = missionService.getAllMissions();

        assert res.size() == 3 && res.get(0).getId() == 3 && res.get(1).getId() == 2 && res.get(2).getId() == 1;
    }

    @Test
    public void getMissionByIdNotExisting() {
        MissionModel res = missionService.getMissionById(2);

        assert res == null;
    }

    @Test
    public void getMissionById() {
        MissionModel res = missionService.getMissionById(1);

        assert res.getId().equals(1);
    }

    @Test
    public void missionFilterithOneKeyword() {
        List<MissionModel> missions = new ArrayList<>();
        createMockMissionsList(missions);

        List<MissionModel> filteredMissions = new ArrayList<>();

        // 7 occurrences => 1 * 7 - 0.5 * 1 * 7 = 3.5
        filteredMissions.add(new MissionModel(10,
                "Paris test",
                "7 MOIS test",
                "Tarif non renseigné test",
                "24% test",
                "1 août 2022 test",
                "Unknown test",
                null,
                "Unknown test"));

        // 3 occurrences => 1 * 3 - 0.5 * 1 * 3 = 1.5
        filteredMissions.add(new MissionModel(9,
                "Paris",
                "7 MOIS",
                "Tarif non renseigné",
                "24%",
                "1 août 2022",
                null,
                "Projet .NET (avec tests)",
                "Créez une application web quelconque en utilisant le framework .NET ainsi que des tests pour tester cette dernière"));

        // 2 occurrences => 1 * 2 - 0.5 * 1 * 2 = 1
        filteredMissions.add(new MissionModel(2,
                "Paris",
                "1 MOIS",
                "10000 euros",
                "50%",
                "12 mai 2021",
                "Développeur c++",
                "Projet initiation au C++ avec tests en appliquant les principes du cours socra",
                "Création calculatrice avec grands nombres pour les tests"));

        // 1 occurrence => 1 * 1 - 0.5 * 1 * 1 = 0.5
        filteredMissions.add(new MissionModel(6,
                "Paris",
                "1 AN",
                "1000 euros",
                "24%",
                "5 août 2021",
                "Développeur frontend",
                "Développement site jeu vidéo et tests",
                "Vous allez devoir afficher sur une page web tous les jeux accessibles par les utilisateurs."));

        // 0 occurrence => 1 * 0 - 0.5 * 1 * 0 = 0
        filteredMissions.add(new MissionModel(1,
                "Issy-les-Moulineaux",
                "12 MOIS",
                "Tarif non renseigné",
                "100%",
                "ASAP",
                "Développeur ReactJS",
                null,
                "Intégration au sein de l’équipe Engineering du pôle Industrialisation"));

        // 0 occurrence => 1 * 0 - 0.5 * 1 * 0 = 0
        filteredMissions.add(new MissionModel(3,
                "Le Kremlin-Bicêtre",
                "1 MOIS",
                "666 euros",
                "0%",
                "2 novembre 2019",
                "Développeur C",
                "Projet 42sh",
                "Réalisation shell personnel"));

        // 0 occurrence => 1 * 0 - 0.5 * 1 * 0 = 0
        filteredMissions.add(new MissionModel(4,
                "Villejuif",
                "3 jours",
                "Rien",
                "100%",
                "3 mars 2020",
                "Développeur Java",
                "Rush Java",
                "Réalisation WebService"));

        // 0 occurrence => 1 * 0 - 0.5 * 1 * 0 = 0
        filteredMissions.add(new MissionModel(5,
                "Toulon",
                "1 MOIS",
                "Relaxation",
                "24%",
                "1 août 2020",
                "Etudiant ing2",
                "Vacances",
                "Se reposer au soleil"));

        // 0 occurrence => 1 * 0 - 0.5 * 1 * 0 = 0
        filteredMissions.add(new MissionModel(7,
                "Bordeaux",
                "10 ANS",
                "500 euros",
                "24%",
                "1 août 2012",
                "Bagnard",
                "Emprisonné dans une galère",
                "Vous devez ramer sans vous arrêter pendant des heures."));

        // 0 occurrence => 1 * 0 - 0.5 * 1 * 0 = 0
        filteredMissions.add(new MissionModel(8,
                "Sevran/Paris",
                "5 MOIS",
                "10 tickets restaurants",
                "24%",
                "10 août 2021",
                "Développeur backend",
                "Projet SOCRA",
                "Vous allez devoir vous occuper de la connexion avec la base de données pour le projet de SOCRA."));


        String[] keywords = {"test"};

        var res = MissionService.filterList(missions, keywords);

        assert res.equals(filteredMissions);
    }

    @Test
    public void withMultipleKeywords() {
        List<MissionModel> missions = new ArrayList<>();
        createMockMissionsList(missions);

        String[] keywords = {"test", "socra", "paris"};

        List<MissionModel> filteredMissions = new ArrayList<>();

        // 7 occurrences test + 0 occurrence socra + 1 occurrence Paris = 8 occurrences => 2 * 8 - 0.5 * 3 * 8 = 8
        filteredMissions.add(new MissionModel(10,
                "Paris test",
                "7 MOIS test",
                "Tarif non renseigné test",
                "24% test",
                "1 août 2022 test",
                "Unknown test",
                null,
                "Unknown test"));

        // 2 occurrences test + 1 occurrence socra + 1 occurrence paris = 4 occurrences => 3 * 4 - 0.5 * 3 * 4 = 6
        filteredMissions.add(new MissionModel(2,
                "Paris",
                "1 MOIS",
                "10000 euros",
                "50%",
                "12 mai 2021",
                "Développeur c++",
                "Projet initiation au C++ avec tests en appliquant les principes du cours socra",
                "Création calculatrice avec grands nombres pour les tests"));

        // 3 occurrences test + 0 occurrence socra + 1 occurrence paris = 4 occurrences => 2 * 4 - 0.5 * 3 * 4 = 2
        filteredMissions.add(new MissionModel(9,
                "Paris",
                "7 MOIS",
                "Tarif non renseigné",
                "24%",
                "1 août 2022",
                null,
                "Projet .NET (avec tests)",
                "Créez une application web quelconque en utilisant le framework .NET ainsi que des tests pour tester cette dernière"));

        // 0 occurrence test + 2 occurrences socra + 1 occurrence paris = 3 occurrences => 2 * 3 - 0.5 * 3 * 3 = 1.5
        filteredMissions.add(new MissionModel(8,
                "Sevran/Paris",
                "5 MOIS",
                "10 tickets restaurants",
                "24%",
                "10 août 2021",
                "Développeur backend",
                "Projet SOCRA",
                "Vous allez devoir vous occuper de la connexion avec la base de données pour le projet de SOCRA."));

        // 1 occurrence test + 0 occurrence socra + 1 occurrence paris = 2 occurrences => 2 * 2 - 0.5 * 3 * 2 = 1
        filteredMissions.add(new MissionModel(6,
                "Paris",
                "1 AN",
                "1000 euros",
                "24%",
                "5 août 2021",
                "Développeur frontend",
                "Développement site jeu vidéo et tests",
                "Vous allez devoir afficher sur une page web tous les jeux accessibles par les utilisateurs."));

        // 0 occurrence test + 0 occurrence socra + 0 occurrence paris = 0 occurrence => 0 * 0 - 0.5 * 0 * 0 = 0
        filteredMissions.add(new MissionModel(1,
                "Issy-les-Moulineaux",
                "12 MOIS",
                "Tarif non renseigné",
                "100%",
                "ASAP",
                "Développeur ReactJS",
                null,
                "Intégration au sein de l’équipe Engineering du pôle Industrialisation"));

        // 0 occurrence test + 0 occurrence socra + 0 occurrence paris = 0 occurrence => 0 * 0 - 0.5 * 0 * 0 = 0
        filteredMissions.add(new MissionModel(3,
                "Le Kremlin-Bicêtre",
                "1 MOIS",
                "666 euros",
                "0%",
                "2 novembre 2019",
                "Développeur C",
                "Projet 42sh",
                "Réalisation shell personnel"));

        // 0 occurrence test + 0 occurrence socra + 0 occurrence paris = 0 occurrence => 0 * 0 - 0.5 * 0 * 0 = 0
        filteredMissions.add(new MissionModel(4,
                "Villejuif",
                "3 jours",
                "Rien",
                "100%",
                "3 mars 2020",
                "Développeur Java",
                "Rush Java",
                "Réalisation WebService"));

        // 0 occurrence test + 0 occurrence socra + 0 occurrence paris = 0 occurrence => 0 * 0 - 0.5 * 0 * 0 = 0
        filteredMissions.add(new MissionModel(5,
                "Toulon",
                "1 MOIS",
                "Relaxation",
                "24%",
                "1 août 2020",
                "Etudiant ing2",
                "Vacances",
                "Se reposer au soleil"));

        // 0 occurrence test + 0 occurrence socra + 0 occurrence paris = 0 occurrence => 0 * 0 - 0.5 * 0 * 0 = 0
        filteredMissions.add(new MissionModel(7,
                "Bordeaux",
                "10 ANS",
                "500 euros",
                "24%",
                "1 août 2012",
                "Bagnard",
                "Emprisonné dans une galère",
                "Vous devez ramer sans vous arrêter pendant des heures."));

        var res = MissionService.filterList(missions, keywords);

        assert res.equals(filteredMissions);
    }

    @Test
    public void withNullorNoKeyword() {
        List<MissionModel> missions = new ArrayList<>();
        createMockMissionsList(missions);

        List<MissionModel> filteredMissions = new ArrayList<>(missions);

        var res = MissionService.filterList(missions, null);
        assert res.equals(filteredMissions);

        String[] keywords = {""};
        res = MissionService.filterList(missions, keywords);
        assert res.equals(filteredMissions);
    }
}
