#
# Build stage
#
FROM maven:3.8.1-openjdk-16-slim AS build
COPY socra_project/src /home/app/src
COPY socra_project/pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

#
# Package stage
#
FROM openjdk:18-jdk-slim
COPY --from=build /home/app/target/socra_project-1.0.0-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]